#coding: utf-8
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup, NavigableString
import requests
import sys
import json
import re
import datetime
from urllib.parse import urlparse


def get_article_details_prothomalo(url):
    image_url = None
    prothomalo_article_details = {}
    try:
        page = requests.get(url)
        soup = BeautifulSoup(page.content, 'lxml')
        try:
            title = "".join([p.text for p in soup.find_all('h1', class_='title mb10')])
            title = title.encode('utf-8')
            print(title)
        except:
            return None
        try:
            published_date = "".join([p.text for p in soup.find_all('span', itemprop='datePublished')])
            published_date = published_date.encode('utf-8')
            print(published_date)
        except:
            return None
        try:
            image_details = soup.find_all('img', class_='jwMediaContent image aligncenter')
            if len(image_details) == 0:
                image_details = soup.find_all('img', class_='jwMediaContent image alignleft')
            for i in image_details:
                try:
                    image_url = i['src']
                    print("image_url ---- >>> ", image_url)
                    if image_url is None:
                        return None
                except:
                    return None
            print("direct ----------------->>>>>>>>>>> ", image_url)
        except:
            return None
        try:
            article_desc = "".join([p.text for p in soup.find_all('div', itemprop='articleBody')])
            article_desc = article_desc.replace("\n", "<br/>").replace("\r", "").replace("\t", "").replace("<br>",
                                                                                                           "<br/>")
            article_desc = article_desc.encode('utf-8')
            print(article_desc.strip())
        except:
            return None

        prothomalo_article_details = {
            'title': title,
            'image_url': image_url,
            'published_date': published_date,
            'description': article_desc,
            # 'logo': self.newsppr_urls[0]['prothomalo_logo_url'],
            # 'newspaper_name': 'prothomalo',
            'url': url,
        }

    except:
        print(sys.exc_info())
    return prothomalo_article_details


def is_absolute(url):
    return bool(urlparse(url).netloc)

url = "https://www.prothomalo.com/bangladesh/article/1650583"
get_article_details_prothomalo(url)


